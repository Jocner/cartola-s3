import express from 'express';
import fileUpload from 'express-fileupload'
import cors from 'cors';
import morgan from 'morgan';
import cartolaRoute from './routes/cartola.route.js';

// import axios from 'axios';
// import conectarDB from './config/db';
// import userRouter from './routes/indicador.routes';
// import swaggerUI from 'swagger-ui-express';
// import swaggerJsDocs from 'swagger-jsdoc';
// import { option } from './swaggerOptions';



const app = express();

// conectarDB();

const PORT = process.env.PORT || 9000;

app.use(cors());
app.use(express.json());
app.use(morgan("dev"));


// const specs = swaggerJsDocs(option);
 
app.use(cartolaRoute);
// app.use('/', swaggerUI.serve, swaggerUI.setup(specs));


app.listen(PORT, () => console.log(`The server is running on port ${PORT}`));
