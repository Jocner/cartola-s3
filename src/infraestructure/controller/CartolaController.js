// // import {getRepository} from "typeorm";
// import {NextFunction, Request, Response} from "express";
// import { uploadFile } from '../service/s3'
// // import {User} from "../entity/User";

// export class CartolaController {


//   async files(req: Request, res: Response, next: NextFunction) {
//     try {

  
//       const service = await uploadFile(req)
//         console.log(service);
      
//       return service

//     }catch(err) {

//         console.log(err);

//     }
//   }
// }  

  // async one(request: Request, response: Response, next: NextFunction) {
  //   return this.userRepository.findOne(request.params.id);
  // }

  // async save(request: Request, response: Response, next: NextFunction) {
  //   return this.userRepository.save(request.body);
  // }

  // async remove(request: Request, response: Response, next: NextFunction) {
  //   let userToRemove = await this.userRepository.findOne(request.params.id);
  //   if (!userToRemove) throw new Error('User not found');
  //   await this.userRepository.remove(userToRemove);
  // }
// }






import { uploadFile, getFiles, getFile } from '../service/s3.js'

// const amazon = require('../service/s3')


export const files = async(req, res) => {
    try {

        // const indicador = await (await axios.get('https://mindicador.cl/api')).data;
        // console.log(req.files);
     
        const service = await uploadFile(req.files.file)


        console.log(service);
        res.json({message: 'File S3'});
        // return service

    }catch(err) {

      console.log(`aqui catch ${err}`);

    }
};

export const allFiles = async(req, res) => {
  try {

      const service = await getFiles()
   

      console.log(service);
      res.json({service});

  }catch(err) {

      console.log(`aqui catch ${err}`);

  }
};

export const file = async(req, res) => {
  // try {
      // const tipo = 'LIPAGINTCCT.TXT'

      // const service = await getFile(tipo)
      const service = await getFile(req.body.filename)
   

      console.log("respuesta funcion controlador", service);
      res.json({service});

  // }catch(err) {

  //     console.log(`aqui catch ${err}`);

  // }
};
