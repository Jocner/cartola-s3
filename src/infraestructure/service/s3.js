import { S3Client, PutObjectCommand, ListObjectsCommand, GetObjectCommand } from '@aws-sdk/client-s3'
import { AWS_BUCKET_REGION, AWS_PUBLIC_KEY, AWS_SECRET_KEY, AWS_BUCKET_NAME } from '../../config.js'
import {getSignedUrl} from '@aws-sdk/s3-request-presigner'
import fs from 'fs'


export const client = new S3Client({
    region: AWS_BUCKET_REGION,
    credentials: {
        accessKeyId: AWS_PUBLIC_KEY,
        secretAccessKey: AWS_SECRET_KEY
    }
})

// export class S3 {

//     async uploadFile(file: any) {

//         const stream = fs.createReadStream(file.tempFilePath)
//         const uploadParams = {
//         Bucket: AWS_BUCKET_NAME,
//         Key: file.name,
//         Body: stream
//         }
//         const command = new PutObjectCommand(uploadParams)
//         return await client.send(command)

//     }

//     async getFiles() {

//         const command = new ListObjectsCommand({
//             Bucket: AWS_BUCKET_NAME
//         })
//         return await client.send(command)

//     }

// }    

export async function uploadFile(file){

    
    const stream = fs.createReadStream(file.tempFilePath)
    const uploadParams = {
        Bucket: AWS_BUCKET_NAME,
        Key: file.name,
        Body: stream
    }
    const command = new PutObjectCommand(uploadParams)
    return await client.send(command)
}

export async function getFiles() {
    // const command = new ListObjectsCommand({
    //     Bucket: AWS_BUCKET_NAME
    // })
    // return await client.send(command)
    const data = [{type: 'S3'}, {type: 'Lambda'}, {type: 'C2'}, {type: 'DynamonDB'}]
    const info = data.map(d =>  d.type )
    console.log('desde service s3')
    console.log(data)

    return info
}

export async function getFile(filename) {

    const command = new GetObjectCommand({
        Bucket: AWS_BUCKET_NAME,
        Key: filename
    })
    
    // // Key: filename
    const result = await (await client.send(command))
    // const result = await client.send(new GetObjectCommand({
    //     Bucket: AWS_BUCKET_NAME,
    //     Key: filename
    // }))
    // return await client.send(command)
    console.log('lectura', filename)

    // console.log(result)
    result.Body.pipe(fs.createWriteStream(`./images/${filename}`))

    // const txt = fs.readFile(`./images/${filename}`, function(err, data) {
    //     if (! err) {
    //       console.log(data.toString());
          
    //     }
    //   });
    let archivo = fs.readFileSync(`./images/${filename}`, 'utf-8');
    //   fs.unlinkSync(`./images/${filename}`)

      console.log('archivo', archivo)

    return archivo
}