import { Router } from 'express';
const router = Router();
import fileUpload from 'express-fileupload'
import { files, allFiles, file } from '../infraestructure/controller/CartolaController.js';


router.use(fileUpload({
    useTempFiles: true,
    tempFileDir: './uploads'
}))



router.post('/files', files
)

// router.post('/allfiles', allFiles
// )

router.get('/allfiles', allFiles
)


// router.post('/file/:filename', file
// )

router.post('/file', file
)


export default router;

